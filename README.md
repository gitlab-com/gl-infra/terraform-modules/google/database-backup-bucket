# GitLab.com Database Backup Bucket Terraform Module

## What is this?

This module provisions the resources necessary to store backups from PostgreSQL instances.

It creates the following resources:

- A GCS bucket with the correct defaults for versioning, lifecycle settings and encryption
- The IAM policies necessary to provide read and write access to a set of given `var.backup_readers` and `var.backup_writers`. 

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | >= 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_storage_bucket.postgres-backup](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket_iam_member.postgres-backup-legacy-bucket-reader](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.postgres-backup-object-admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.postgres-backup-object-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_backup_readers"></a> [backup\_readers](#input\_backup\_readers) | IAM entities that have permission to read from the backup bucket | `set(string)` | n/a | yes |
| <a name="input_backup_writers"></a> [backup\_writers](#input\_backup\_writers) | IAM entities that have permission to write to the backup bucket | `set(string)` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_kms_key_id"></a> [kms\_key\_id](#input\_kms\_key\_id) | n/a | `string` | n/a | yes |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_lifecycle_settings"></a> [lifecycle\_settings](#input\_lifecycle\_settings) | The lifecycle settings for managing object retention and lifecycle management across multiple storage classes:<br/><br/>default\_storage\_class\_retention     The age (in days) after which objects are moved to Nearline storage class, if nearline\_enabled is set to false objects are moved straight to Coldline.<br/><br/>minimum\_retention                   The time (in seconds) that objects in the bucket must be retained and cannot be<br/>                                    deleted, overwritten, or archived. The value must be less than the value of default\_storage\_class\_retention (converted to seconds) or 2,147,483,647 seconds, whichever is smaller.<br/><br/>nearline\_retention                  The age (in days) after which objects are moved to Coldline storage class. The<br/>                                    value must be greater than `default_storage_class_retention` and null if nearline\_enabled is false.<br/><br/>nearline\_enabled                    When true, will enable transitioning to nearline storage before Coldline.<br/><br/>retention\_policy\_enabled            When true, will add a `retention_policy` to the bucket to define the<br/>                                    `minimum_retention` period for objects.<br/><br/>retention\_policy\_is\_locked          When true, the bucket will be locked and permanently restrict edits to the<br/>                                    bucket's retention policy. CAUTION: Locking a bucket is an irreversible action.<br/><br/>total\_retention                     The age (in days) after which objects are deleted. This must be greater than<br/>                                    `nearline_retention` | <pre>object({<br/>    default_storage_class_retention = optional(number, 14)<br/>    minimum_retention               = optional(number, 13 * 24 * 60 * 60) # 13 days as seconds<br/>    nearline_retention              = optional(number, 40)<br/>    nearline_enabled                = optional(bool, true)<br/>    retention_policy_enabled        = optional(bool, false)<br/>    retention_policy_is_locked      = optional(bool, false)<br/>    total_retention                 = optional(number, 120)<br/>  })</pre> | `{}` | no |
| <a name="input_storage_class"></a> [storage\_class](#input\_storage\_class) | n/a | `string` | `"MULTI_REGIONAL"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_bucket_location"></a> [bucket\_location](#output\_bucket\_location) | The bucket's location |
| <a name="output_bucket_name"></a> [bucket\_name](#output\_bucket\_name) | The bucket's name |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
