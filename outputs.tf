output "bucket_name" {
  value       = google_storage_bucket.postgres-backup.name
  description = "The bucket's name"
}

output "bucket_location" {
  value       = google_storage_bucket.postgres-backup.location
  description = "The bucket's location"
}
