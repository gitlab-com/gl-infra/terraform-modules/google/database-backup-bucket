resource "google_storage_bucket" "postgres-backup" {
  name = "gitlab-${var.environment}-postgres-backup"

  labels = merge(var.labels, {
    name      = "gitlab-${var.environment}-postgres-backup"
    tfmanaged = "yes"
  })

  location = "us"

  storage_class = var.storage_class

  uniform_bucket_level_access = true

  versioning {
    enabled = false
  }

  dynamic "lifecycle_rule" {
    for_each = var.lifecycle_settings.nearline_enabled ? [1] : []
    content {
      action {
        type          = "SetStorageClass"
        storage_class = "NEARLINE"
      }

      condition {
        age        = var.lifecycle_settings.default_storage_class_retention
        with_state = "ANY"
      }
    }
  }

  lifecycle_rule {
    action {
      type          = "SetStorageClass"
      storage_class = "COLDLINE"
    }
    # If no nearline retention is set immediately transition to coldline 
    condition {
      age        = var.lifecycle_settings.nearline_enabled ? var.lifecycle_settings.nearline_retention : var.lifecycle_settings.default_storage_class_retention
      with_state = "ANY"
    }
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = var.lifecycle_settings.total_retention
      with_state = "ANY"
    }
  }

  dynamic "retention_policy" {
    for_each = var.lifecycle_settings.retention_policy_enabled ? [1] : []
    content {
      is_locked        = var.lifecycle_settings.retention_policy_is_locked
      retention_period = var.lifecycle_settings.minimum_retention
    }
  }

  logging {
    log_bucket = "gitlab-${var.environment}-storage-logs"
  }

  dynamic "encryption" {
    for_each = var.kms_key_id == "" ? [] : [1]
    content {
      default_kms_key_name = var.kms_key_id
    }
  }
}

# These bindings are for the wal-e process that pushes and pulls WAL archive/snapshots
# Unfortunately, wal-e also needs to overwrite objects (after a basebackup is finished, it re-pushes a file).
# However, we have a retention policy in place that prevents objects from being permanently deleted.
resource "google_storage_bucket_iam_member" "postgres-backup-object-admin" {
  for_each = var.backup_writers

  bucket = google_storage_bucket.postgres-backup.name
  role   = "roles/storage.objectAdmin"
  member = each.value
}

resource "google_storage_bucket_iam_member" "postgres-backup-object-viewer" {
  for_each = setunion(
    var.backup_writers,
    var.backup_readers
  )

  bucket = google_storage_bucket.postgres-backup.name
  role   = "roles/storage.objectViewer"
  member = each.value
}

resource "google_storage_bucket_iam_member" "postgres-backup-legacy-bucket-reader" {
  for_each = setunion(var.backup_writers, var.backup_readers)

  bucket = google_storage_bucket.postgres-backup.name
  role   = "roles/storage.legacyBucketReader"
  member = each.value
}
