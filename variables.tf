variable "environment" {
  type        = string
  description = "The environment name"
}

variable "backup_writers" {
  type        = set(string)
  description = "IAM entities that have permission to write to the backup bucket"
}

variable "backup_readers" {
  type        = set(string)
  description = "IAM entities that have permission to read from the backup bucket"
}

variable "kms_key_id" {
  type = string
}

variable "lifecycle_settings" {
  type = object({
    default_storage_class_retention = optional(number, 14)
    minimum_retention               = optional(number, 13 * 24 * 60 * 60) # 13 days as seconds
    nearline_retention              = optional(number, 40)
    nearline_enabled                = optional(bool, true)
    retention_policy_enabled        = optional(bool, false)
    retention_policy_is_locked      = optional(bool, false)
    total_retention                 = optional(number, 120)
  })


  # The retention policy on the bucket prevents deletion of objects in the bucket before `minimum_retention` seconds.
  # This value must be less than `default_storage_class_retention`, as it prevents objects from being archived until
  # they are older than `minimum_retention`
  # See https://cloud.google.com/storage/docs/bucket-lock and https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket#retention_period
  validation {
    // Convert default_storage_class_retention to seconds for comparison with minimum_retention
    condition     = var.lifecycle_settings.minimum_retention < (var.lifecycle_settings.default_storage_class_retention * 24 * 60 * 60)
    error_message = "The `minimum_retention` value must be less than `default_storage_class_retention`."
  }

  validation {
    condition     = var.lifecycle_settings.nearline_enabled == false || var.lifecycle_settings.default_storage_class_retention < var.lifecycle_settings.nearline_retention
    error_message = "The `default_storage_class_retention` value must be less than `nearline_retention` when nearline is enabled."
  }

  # Nearline has a minimum storage duration of 30 days, so we let the object stay there for most of the duration.
  # See https://cloud.google.com/storage/pricing#archival-pricing
  validation {
    condition     = var.lifecycle_settings.nearline_enabled == false || var.lifecycle_settings.nearline_retention > 30 && var.lifecycle_settings.nearline_retention < var.lifecycle_settings.total_retention
    error_message = "The `nearline_retention` value must be greater than 30 days and less than `total_retention` when nearline is enabled."
  }


  # Nearline has a minimum storage duration of 30 days, so we let the object stay there for most of the duration.
  # See https://cloud.google.com/storage/pricing#archival-pricing
  validation {
    condition     = var.lifecycle_settings.default_storage_class_retention < var.lifecycle_settings.total_retention
    error_message = "The `default_storage_class_retention` value must be less than `total_retention`."
  }

  # Coldline has a minimum storage duration of 90 days, so we let the object stay there for most of the duration.
  # See https://cloud.google.com/storage/pricing#archival-pricing
  validation {
    condition     = var.lifecycle_settings.total_retention >= 90
    error_message = "The `total_retention` value must be greater than 90 days."
  }


  description = <<-END
    The lifecycle settings for managing object retention and lifecycle management across multiple storage classes:

    default_storage_class_retention     The age (in days) after which objects are moved to Nearline storage class, if nearline_enabled is set to false objects are moved straight to Coldline.

    minimum_retention                   The time (in seconds) that objects in the bucket must be retained and cannot be
                                        deleted, overwritten, or archived. The value must be less than the value of default_storage_class_retention (converted to seconds) or 2,147,483,647 seconds, whichever is smaller.

    nearline_retention                  The age (in days) after which objects are moved to Coldline storage class. The
                                        value must be greater than `default_storage_class_retention` and null if nearline_enabled is false.

    nearline_enabled                    When true, will enable transitioning to nearline storage before Coldline.

    retention_policy_enabled            When true, will add a `retention_policy` to the bucket to define the
                                        `minimum_retention` period for objects.

    retention_policy_is_locked          When true, the bucket will be locked and permanently restrict edits to the
                                        bucket's retention policy. CAUTION: Locking a bucket is an irreversible action.

    total_retention                     The age (in days) after which objects are deleted. This must be greater than
                                        `nearline_retention`
    END

  default = {}
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}
